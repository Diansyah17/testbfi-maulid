package com.bfi.dian;

public class Mask {

	public String mask(String original) {
		String[] splits = original.split(" ");
		StringBuilder ret = new StringBuilder();
		for (String split: splits) {
			int i = 1;
			ret.append(split.substring(0, i));
			for (; i<split.length() - 1; i++) {
				ret.append("*");
			}
			
			if (i < split.length()) {
				ret.append(split.substring(i, split.length()));
			}
			
			ret.append(' ');
		}
		
		return ret.toString().trim();
	}
	
	public static void main(String[] args) {
		Mask mask = new Mask();
		String result = mask.mask("El Taurino");
		System.out.println(result);
	}
}
