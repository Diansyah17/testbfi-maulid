package com.bfi.dian;

import java.io.FileReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Cities {

	public String findCities(String data) throws Exception {
		Reader rdr = new FileReader("cities.json");
		Writer wrt = new StringWriter();
		IOUtils.copy(rdr, wrt);
		rdr.close();
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		City[] cities = gson.fromJson(wrt.toString(), City[].class);
		
		
		String regex = ".*" + data.replaceAll("[AEIOUaeiou]", ".") + ".*";
		// System.out.println("Data: " + regex);
		StringBuilder output = new StringBuilder();
		for (City city: cities) {
			// System.out.println(city.getName());
			if (!"ID".equalsIgnoreCase(city.getCountry())) {
				continue;
			}
			
			if (!city.getName().toLowerCase().matches(regex.toLowerCase())) {
				continue;
			}
			
			output.append(',');
			output.append(city.getName()).append(' ');
		}
		
		output.delete(0,  1);
		return output.toString().replace(" ,", ", ");
	}
	
	public static void main(String[] args) throws Exception {
		Scanner inp = new Scanner(System.in);
		System.out.print("Input: ");
		String data = inp.nextLine();
		inp.close();
		
		Cities cities = new Cities();
		String out = cities.findCities(data);
		System.out.println("Output: " + out);
		
	}
	
}
